# acp3/skeleton-app
This repository holds the skeleton app for the ACP3.

## Installation

To install the skeleton app, ensure that you have Composer already installed.

If so, then run the following command, which will get all the required packages to get the ACP3 CMS to work:
```sh
$ composer create-project acp3/skeleton-app <directory>
```

## License

This project is licensed under the terms of the [GPL 2.0+](https://github.com/ACP3/skeleton-app/blob/master/LICENSE).
